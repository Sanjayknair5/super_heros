class SuperHero

  SuperList = {
    "batman": ["can_throw_punches","operates_at_night"],
    "superman": ["can_fly","can_throw_punches"],
    "wonder_woman": ["can_throw_punches","can_ride_horses"],
    "spider_man": ["can throw punches","can_shoot_web","can_climb_buildings"]
  }

  SuperPowers = [ "can_throw_punches", "operates_at_night", "can_fly", "can_throw_punches",
                  "can_throw_punches", "can_ride_horses", "can throw punches", "can_shoot_web",
                  "can_climb_buildings", "can_jump", "can_eat_bullet"
                  ]

  attr_accessor :type

  def initialize(arg)
    self.type = arg
  end



  def type_key
    self.type.gsub(" ","_").downcase
  end


  def self.add_power(power_input)
    power = power_input.gsub(" ","_").downcase
    define_method "#{power}?" do
      list = SuperHero::SuperList[type_key.to_sym]
      if list.nil?
        return false
      else
        return list.include?(power)
      end
    end
  end


  def self.add_hero(name,power_array)
    if name.kind_of?(String) and power_array.kind_of?(Array)
      SuperList[name.gsub(" ","_").downcase.to_sym] = power_array
      power_array.each do |power|
        add_power(power) unless instance_methods.include?("#{power}?".to_sym)
      end
    else
      return "Invalid Input"
    end
  end



  SuperPowers.each do |power|
    SuperHero.add_power(power)
  end

end
