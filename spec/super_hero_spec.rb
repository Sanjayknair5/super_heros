require './super_hero'

describe SuperHero do 
  context "with normal input" do
    it "should return super power status" do
      s = SuperHero.new("wonder woman")
      expect(s.can_throw_punches?).to eq true
      expect(s.can_climb_buildings?).to eq false
      expect(s.can_ride_horses?).to eq true
      expect(s.can_eat_bullet?).to eq false
    end
  end


  context "add new power " do
    it "should add a power and return status" do
      SuperHero.add_power("sleep")
      s = SuperHero.new("wonder woman")
      expect(s.can_throw_punches?).to eq true
      expect(s.can_climb_buildings?).to eq false
      expect(s.can_ride_horses?).to eq true
      expect(s.can_eat_bullet?).to eq false
      expect(s.sleep?).to eq false
    end
  end


  context "add new super hero test " do
    it "should add a super hero and return status" do
      SuperHero.add_hero("Ironman",["can_shoot"])
      s = SuperHero.new("Ironman")
      expect(s.can_shoot?).to eq true
      expect(s.can_climb_buildings?).to eq false
    end
  end
end    