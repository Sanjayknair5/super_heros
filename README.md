# super_heros

To run the program, navigate to home folder and then open ruby console (irb) .
	irb
	require "./super_hero"
	
	Following Functions are available in the program : 
		1. Initialize new super hero and check power status 
			s = Superhero.new('wonder woman')
			s.can_throw_punches? => true
			
		2. Add new super power
			SuperHero.add_power("sleep")
		
		3. Add new super hero and its power
			SuperHero.add_hero("Ironman",["can_shoot"])
	
To run test case execute command : rspec
	

